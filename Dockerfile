FROM php:8.2-fpm

# install necessary dependencies
RUN apt-get update && apt-get install -y \
    libzip-dev \
    unzip \
    libpng-dev \
    libcurl4-openssl-dev \
    libxml2-dev \
    libsodium-dev \
    libxslt-dev \
    libjpeg-dev \
    libfreetype6-dev \
    libonig-dev

RUN pecl install -o -f igbinary \
    &&  rm -rf /tmp/pear \
    &&  echo "extension=igbinary.so" > /usr/local/etc/php/conf.d/igbinary.ini

RUN pecl install -o -f --configureoptions 'enable-redis-igbinary="yes"' redis \
    &&  rm -rf /tmp/pear \
    &&  echo "extension=redis.so" > /usr/local/etc/php/conf.d/redis.ini

RUN docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/  \
    && docker-php-ext-enable  \
    igbinary  \
    redis \
    && docker-php-ext-install  \
    opcache \
    mbstring \
    gd  \
    bcmath  \
    ctype  \
    curl  \
    dom  \
    fileinfo  \
    iconv  \
    intl  \
    pdo  \
    pdo_mysql  \
    simplexml  \
    soap  \
    sockets  \
    sodium  \
    xml  \
    xsl  \
    xmlwriter  \
    zip

COPY ./config/99-config-php.ini /usr/local/etc/php/conf.d/99-config-php.ini

# Install npm
RUN apt-get update  \
    && apt-get install -y nodejs gnupg \
    && mkdir -p /etc/apt/keyrings \
    && curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg \
    && echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_20.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list \
    && apt-get update \
    && apt-get install nodejs -y \
    && php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php composer-setup.php --install-dir=/usr/local/bin --filename=composer \
    && php -r "unlink('composer-setup.php');"
